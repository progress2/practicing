import React, { useState } from 'react';
import { View, StyleSheet, TextInput, Button, Modal } from 'react-native';

const ValueInput = props => {
    const [EnteredValue, setEnteredValue] = useState('');
    const InputHandler = (enteredText) => 
    {setEnteredValue(enteredText)};

    const addValHandler = () => {
        props.onAddValue(EnteredValue);
        setEnteredValue('')
    };

    return (
        <Modal
        visible = {props.visible}
        animationType= 'slide'>
            <View style={styles.InputContainer}>
                <TextInput
                placeholder= "Input"
                style={styles.Input}
                onChangeText={InputHandler}
                value={EnteredValue}/>
              <View
              style={styles.ButtMain}>
               <View style={styles.ButtStyle}>     
                <Button
                title="ADD"
                onPress={addValHandler}/>
               </View>
               <View style={styles.ButtStyle}> 
                <Button
                title="Cancel"
                color= "red"
                onPress= {props.onCancel}/>
               </View> 
              </View>  
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    InputContainer: {
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    Input: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 5,
        width: '80%'
    },
    ButtMain: {
        width: '60%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 4
    },
    ButtStyle: {
        width: '40%'
    },
});

export default ValueInput;